<?php get_header(); ?>

  <div class="container mt-5">
    <div class="row">
      <div class="col-lg-6">
        <div class="coffee_txt">
          <div class="coffee_btn" onclick="getCoffee('Espresso',47)"></div>
          <span>Espresso - 47 rub.</span>
        </div>
        <div class="coffee_txt">
          <div class="coffee_btn" onclick="getCoffee('Frappe',66)"></div>
          <span>Frappe - 66 rub.</span>
        </div>
        <div class="coffee_txt">
          <div class="coffee_btn" onclick="getCoffee('Cappuccino',70)"></div>
          <span>Cappuccino - 70 rub.</span>
        </div>
        <div class="coffee_txt">
          <div class="coffee_btn" onclick="getCoffee('Latte',60)"></div>
          <span>Latte - 60 rub.</span>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="row">
          <div class="col-md-6">
            <div id="display">
              <p id="displayInfo">Deposit money.</p>
              <p id="balance">Balance is 0 rub.</p>
              <div class="progress">
                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width:0%;"></div>
              </div>
            </div>
            <div class="d-flex justify-content-center mt-3">
                <button class="btn btn-info btn-lg" onclick="if (money.value!=0) {getChange(money.value)}">Get change</button>
            </div>
          </div>
          <div class="col-md-6">
            <input type="hidden" placeholder="Сколько у вас денег?" id="money" value="0"/>
            <img src="<?php echo get_template_directory_uri() . '/assets/bill_acc.jpg'; ?>" alt="">
            <div id="changeBox"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3"><img src="<?php echo get_template_directory_uri() . '/assets/50rub.jpg'; ?>" alt=""
          data-bill-value="50"></div>
      <div class="col-md-3"><img src="<?php echo get_template_directory_uri() . '/assets/100rub.jpg'; ?>" alt=""
          data-bill-value="100"></div>
      <div class="col-md-3"><img src="<?php echo get_template_directory_uri() . '/assets/500rub.jpg'; ?>" alt=""
          data-bill-value="500"></div>
    </div>
  </div>
  
<?php get_footer(); ?>