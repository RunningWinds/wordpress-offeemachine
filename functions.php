<?php
function add_css()
{
  wp_register_style('first', get_template_directory_uri() . '/assets/css/style-starter.css', false, '1.1', 'all');
  wp_enqueue_style('first');
  wp_register_style('coffee-bootstrapcss', get_template_directory_uri() . '/assets/bootstrap/css/bootstrap.min.css');
  wp_enqueue_style('coffee-bootstrapcss');
}
add_action('wp_enqueue_scripts', 'add_css');

function add_googleicons()
{
  wp_register_style('outline','https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200');
  wp_enqueue_style('outline');
}
add_action('wp_enqueue_scripts', 'add_googleicons'); 

$args = array(
  'default-color' => 'ffffff',
  'default-image' => get_template_directory_uri() . '/assets/bg.png',
);

add_theme_support('custom-background', $args);