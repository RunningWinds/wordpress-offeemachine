<?php wp_footer(); ?>
<script>
  let money = document.getElementById("money");
  let display = document.getElementById("displayInfo");
  let bills = document.querySelectorAll("img[src$='rub.jpg']");
  let bill_acc = document.querySelector("img[src$='bill_acc.jpg']");
  let progressBar = document.querySelector(".progress-bar");
  let balance = document.getElementById("balance");
  let changeBox = document.getElementById("changeBox");

  bills.forEach(function (bill) {
    bill.onmousedown = function (e) {
      bill = e.currentTarget;
      let shiftX = event.clientX - bill.getBoundingClientRect().left;
      let shiftY = event.clientY - bill.getBoundingClientRect().top;
      bill.style.position = "absolute";
      bill.style.zIndex = 1000;
      bill.style.transform = "rotate(90deg)";
      document.body.append(bill);
      moveAt(event.pageX, event.pageY);

      function moveAt(pageX, pageY) {
        bill.style.left = pageX - shiftX + "px";
        bill.style.top = pageY - shiftY + "px";
      }
      function onMouseMove(event) {
        moveAt(event.pageX, event.pageY);
      }

      document.addEventListener("mousemove", onMouseMove);

      bill.onmouseup = function () {
        document.removeEventListener("mousemove", onMouseMove);
        bill.onmouseup = null;
        bill.style.zIndex = 1;
        let bill_top = bill.getBoundingClientRect().top;
        let bill_left = bill.getBoundingClientRect().left;
        let bill_right = bill.getBoundingClientRect().right;
        let bill_acc_top = bill_acc.getBoundingClientRect().top;
        let bill_acc_left = bill_acc.getBoundingClientRect().left;
        let bill_acc_right = bill_acc.getBoundingClientRect().right;
        let bill_acc_bottom =
          bill_acc.getBoundingClientRect().bottom -
          (bill_acc.getBoundingClientRect().height / 3) * 2 -
          20;

        if (
          bill_top > bill_acc_top &&
          bill_left > bill_acc_left &&
          bill_right < bill_acc_right &&
          bill_top < bill_acc_bottom
        ) {
          bill.classList.add("animated");
          setTimeout(function () {
            bill.hidden = true;
          }, 660);
          money.value = +money.value + +bill.dataset.billValue;
          balance.innerHTML = `Balance is ${money.value} rub.`;
        }
      };
      bill.ondragstart = function () {
        return false;
      };
    };
  });

  function startProgressBar(coffeeName) {
    let i = 0;
    display.innerHTML = `<span class="material-symbols-outlined">local_cafe</span> &nbsp; ${coffeeName} is being brewed...`;
    progressBar.parentElement.hidden = false;
    function progress() {
      i++;
      progressBar.style.width = i + "%";

      if (i == 100) {
        clearInterval(timerId);
        progressBar.parentElement.hidden = true;
        progressBar.style.width = 0 + "%";
        display.innerHTML = `<span class="material-symbols-outlined">coffee</span> &nbsp; ${coffeeName} is ready!`;
      }
    }
    let timerId = setInterval(progress, 100);
  }

  function getCoffee(name, price) {
    cPrice = +price;
    if (progressBar.style.width == "0%") {
      if (money.value >= cPrice) {
        startProgressBar(name);
        money.value -= cPrice;
        balance.innerHTML = `Balance is ${money.value} rub.`;
      } else {
        display.innerHTML = "Not enough money for " + name;
      }
    } 
  }

  function getChange(num) {
    let coin;
    if (num >= 10)
      coin = 10;
    else if (num >= 5)
      coin = 5;
    else if (num >= 2)
      coin = 2;
    else if (num >= 1)
      coin = 1;

    coinAdress = "<?php echo get_template_directory_uri() ?>" + '/assets/' + coin + "rub.png";
    let coinImg = document.createElement("img");
    coinImg.setAttribute("src", coinAdress);
    coinImg.setAttribute("id", "coin");
    coinImg.setAttribute("style",
      `width:60px; 
    left:${getRandom(0, 120)}px; 
    top:${getRandom(0, 130)}px; 
    position:absolute; 
    cursor:pointer;
    opacity:0.9;`);
    coinImg.setAttribute("data-bill-value", coin);

    coinImg.addEventListener('mouseover', function () { coinImg.style.opacity = 1; });
    coinImg.addEventListener('mouseout', function () { coinImg.style.opacity = 0.9; });
    coinImg.addEventListener('click', function () {
      coinImg.hidden = true; money.value = +money.value + +coinImg.dataset.billValue;
      balance.innerHTML = `Balance is ${money.value} rub.`;
    });

    changeBox.appendChild(coinImg);
    money.value -= coin;
    balance.innerHTML = `Balance is ${money.value} rub.`;
    if (num - coin != 0)
      getChange(num - coin);
  }

  function getRandom(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
  }
</script>
</body>

</html>